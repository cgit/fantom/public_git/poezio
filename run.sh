#!/usr/bin/bash

# Before to run, make some check-in:
#   - Use dns server custom or not
#   - Create directory tree:
#     - /contener/poezio-prod
#       - /contener/poezio-prod/config
#       - /contener/poezio-prod/data
#   - Build docker image with tag "poezio:0.14-3.fr"
#   - Check if your Linux user is allowed to run "docker run" command with sudo
docker run -ti -e TERM --dns 192.168.7.1 --log-driver none -e TZ="Europe/Paris" -v /contener/poezio-prod/config:/home/poezio-user/.config/poezio:Z -v /contener/poezio-prod/data:/home/poezio-user/.local/share/poezio:Z -v /container/poezio-prod/proxychains:/home/poezio-user/proxychains:Z  fantomfp/poezio-omemo:0.14-3.fr.x64 /usr/bin/poezio
