FROM fedora:41

# Set the timezone (for logs and history of poezio)
# Comment this line to use UTC timezone
ENV TZ="Europe/Paris"


RUN dnf -y update && dnf clean all


# Add non-official CA to connect to the server
#
# If you want an account on casperlefantom.net, visit the website
# https://chat.casperlefantom.net to create account, then you can
# login with any client, and you can enable OMEMO for end-to-end
# encryption.
#
# or create new account on jabber.fr provider
RUN curl -o /etc/pki/ca-trust/source/anchors/root.pem https://dl.casperlefantom.net/pub/ssl/root.pem &&  \
    /usr/bin/update-ca-trust

# Checksum of server's certificate is available here:
# https://dl.casperlefantom.net/pub/ssl/fingerprint-for-poezio-client.txt
# http://uhxfe4e6yc72i6fhexcpk4ph4niueexpy4ckc3wapazxqhv4isejbnyd.onion/pub/ssl/fingerprint-for-poezio-client.txt


RUN dnf -y install poezio        \
                   poezio-omemo  \
                   proxychains-ng \
&& dnf clean all


# Create user to run process as non-root
RUN useradd -m -s /bin/sh poezio-user
RUN mkdir -p /home/poezio-user/.config/poezio               \
             /home/poezio-user/.local/share/poezio &&       \
chown -R poezio-user:poezio-user /home/poezio-user/.config  \
                                 /home/poezio-user/.local

# Make config tree for proxychains
RUN mkdir /home/poezio-user/proxychains/ &&                                         \
sed -i 's/socks4 	127.0.0.1 9050/socks5 172.17.0.1 9050/' /etc/proxychains.conf &&  \
chown -R poezio-user:poezio-user /home/poezio-user/proxychains

# Switching to non-root user
USER poezio-user
WORKDIR /home/poezio-user/


VOLUME ["/home/poezio-user/.config/poezio", "/home/poezio-user/.local/share/poezio", "/home/poezio-user/proxychains"]
CMD ["/usr/bin/proxychains", "/usr/bin/poezio"]
